#include <stdio.h>
void recurse(int level)
{
	char array[10240];
	printf("%d\n",level);
	recurse(level+1);
}

int main (void)
{
	recurse(1);
	return(0);
}


/*
 * Q1: What should happen when you execute the program?
 * 	   The program should crash the moment it gets filled with too much data.
 *
 * Q4:  Increase the Array Size to 10240. Run it again, what changed?
 *  	What does this tell you about the stack size?
 *  	The size of a stack is finite and cant hande an array of size 10240.
 * */
